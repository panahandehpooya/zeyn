# Zeyn web app

[![pipeline status](https://gitlab.com/panahandehpooya/zeyn/badges/master/pipeline.svg)](https://gitlab.com/panahandehpooya/zeyn/commits/master)

## Demo

If pipeline has passed, an up to date instance is running at [Heroku](zeyn-todo.herokuapp.com)


## Development

### Data structure in Firebase

**A newly created task needs to be pushed to two different locations in Firebase.** The actual task data is stored
in /todo/TASKID/{data}. However, since Firebase doesn't allow quering the database (like could be done in a
relational database), the task ids have to be stored separately as well. They are located under
/users/USERID/tasks/TASKID. To retrieve task data, first the user's all task ids are retrieved, and using
those the individual tasks can be fetched as well.

In JS this is done like this:

```
const todo = {
    'title': title,
    'due': date,
    'completed': 'false',
    'owner': userId
}

const newTodoKey = dbRoot.ref('todo').push().key

var updates = {}
updates['/todo/' + newTodoKey] = todo
updates['/users/' + userId + '/tasks/' + newTodoKey] = 'true'

dbRoot.ref().update(updates)
```


### Notes regarding CI

The CI pipeline consists of three stages:

1. Build - Install dependencies
2. Lint - Eslint is run on all javascript files
3. Deploy - The app is deployed to Heroku
