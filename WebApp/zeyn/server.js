'use strict'

const conf = require('./.firebase_conf.json')
const firebase = require('firebase')

/*
|--------------------------------------------------------------------------
| Http server
|--------------------------------------------------------------------------
|
| This file bootstrap Adonisjs to start the HTTP server. You are free to
| customize the process of booting the http server.
|
| """ Loading ace commands """
|     At times you may want to load ace commands when starting the HTTP server.
|     Same can be done by chaining `loadCommands()` method after
|
| """ Preloading files """
|     Also you can preload files by calling `preLoad('path/to/file')` method.
|     Make sure to pass relative path from the project root.
*/

const { Ignitor } = require('@adonisjs/ignitor')

var config = {
    apiKey: conf['FIREBASE_API_KEY'],
    authDomain: conf['FIREBASE_AUTH_DOMAIN'],
    databaseURL: conf['FIREBASE_DB_URL'],
    storageBucket: conf['FIREBASE_STORAGE_BUCKET']
};

firebase.initializeApp(config);


new Ignitor(require('@adonisjs/fold'))
  .appRoot(__dirname)
  .fireHttpServer()
  .catch(console.error)
