'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route')


Route.on('/').render('index')
Route.on('/signin').render('signin')
Route.on('/contact').render('contact')
Route.on('/support').render('support')

Route.resource('/dashboard', 'TodoController')
