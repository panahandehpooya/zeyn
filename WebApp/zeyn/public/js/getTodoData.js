$(document).ready( () => {
  const dbRoot = firebase.database()

  let readTodoKeys = (userid) => {
    return new Promise( (resolve, reject) => {
      dbRoot.ref('/users/' + userid + '/tasks/')
        .once('value')
        .then( (snapshot) => {
          const keyset = Object.keys(snapshot.val())
          resolve(keyset)
        }).catch( (error) => {
          reject(error)
        })
    })
  }

  let retrieveSingleTodo = (key) => {
    return new Promise( (resolve, reject) => {
      dbRoot.ref('/todo/' + key)
        .once('value')
        .then( snap => {
          const task = {}
          task[snap.key] = snap.val()
          resolve(task)
        }).catch( error => {
          reject(error)
        })
    })
  }

  async function retrieveTodoData(userid) {
    try {
      const todoKeys = await readTodoKeys(userid)
      var promises = []

      for (let key of todoKeys) {
        promises.push(retrieveSingleTodo(key))
      }

      return await Promise.all(promises)

    } catch(error) {
      console.log(error)
      throw error
    }
  }

  /** Retrieving user's todo data happens in three phases:
   *
   * 1. The user has to be authenticated.
   *  - The onAuthStateChanged listener checks that the user is signed in.
   *  - If the user is signed in, the user variable is not null.
   *    - If this is the case, we can proceed to retrieving this user's data
   *
   * 2. The users todo keys are fetched
   *  - The keys are located in:
   *
   *    users : {
   *      <firebase_generated_user_id> : {
   *        tasks : {
   *          <firebase_task_id> : true,
   *          <another_task_id> : true
   *        }
   *      }
   *    }
   *
   *  - Once the keys are saved, the tasks can be retrieved from:
   *
   *    todo : {
   *      <firebase_task_id> : {
   *        title : "Some title",
   *        due : "2018-04-05T00:00:00Z",
   *        ...
   *      },
   *      <another_task_id> : {
   *        ...
   *      }
   *    }
   *
   * 3. The users todo entries are fetched using the previous keys
   *  - The todo entries are saved into an object by the task key:
   *
   *    data : {
   *      <first_task_id> : {
   *        title: "Some title",
   *        ...
   *      },
   *      <second_task_id> : {
   *        ...
   *      }
   *    }
   *
   * 4. Finally the tasks can be rendered to html
   *
   */
  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      const userId = user.uid
      retrieveTodoData(userId)
        .then( data => {
          data.forEach( (elem) => {
            const key = Object.keys(elem)[0]
            const [completed, due, owner, title] = Object.values(elem[key])
            const rowHtml = composeTableRow(title, parseDate(due), completed, key)

            if (completed == 'false' || completed == false) {
              $('#todoTable tbody').append(rowHtml)
            } else {
              $('#doneTable tbody').append(rowHtml)
            }
          })
          $('#taskCount').html(taskCount() + ' tasks')
        }).catch( (error) => {
          // TODO: Handle errors properly, e.g. user not signed in
          console.log(error)
        })
    }
  })

})
