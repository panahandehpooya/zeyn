$(document).ready( () => {

  let currentValue = ''
  const selectTextfieldsExceptNew = 'input[type="text"]:not(#newTitle)'
  const selectDatefieldsExceptNew = 'input[type="date"]:not(#newdue)'
  const selectAllButInputRow = selectTextfieldsExceptNew + ', ' + selectDatefieldsExceptNew

  $(document.body).on('click', 'input[type="checkbox"]', event => {

    const spacer = '<tr class="tableRowSpacer"></tr>'

    // Key is stored in table row's id. Inputs parent is <td> so <tr> is the grandparent
    const key = event.target.parentNode.parentNode.id
    const status = event.target.checked
    const firebasePath = '/todo/' + key
    const data = { 'completed' : status }
    firebase
      .database()
      .ref(firebasePath)
      .update(data)

    const destination = status == true ? '#doneTable tbody' : '#todoTable tbody'

    const $row = $(event.target).parent().parent()
    const $rowSpacer = $row.next('.tableRowSpacer')

    $row.detach().appendTo(destination)
    $rowSpacer.remove()
    $(destination).append(spacer)
  })

  $(document.body).on('click', selectAllButInputRow, event => {
    const key = event.target.parentNode.parentNode.id
    currentValue = event.target.attributes.getNamedItem('value').value
    event.target.readOnly = false
  })

  $(document.body).on('focusout', selectAllButInputRow, event => {
    const key = event.target.parentNode.parentNode.id
    const modifiedValue = $(event.target).val()
    const modifiedField = $(event.target).attr('name')
    const firebasePath = '/todo/' + key
    const data = {}

    // Modifying date field only returns the YYYY-MM-DD part, without the time
    data[modifiedField] =
      modifiedField === 'due' ?
        modifiedValue + 'T00:00:00Z' :
        modifiedValue

    if (modifiedValue !== currentValue) {
      firebase
        .database()
        .ref(firebasePath)
        .update(data)
    }
    event.target.readOnly = true
  })
})
