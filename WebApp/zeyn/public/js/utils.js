const composeTableRow = (title, due, completed, key) => {
  let check = (completed == false || completed == 'false') ? '' : 'checked'
  let string = ''
  string += '<tr id="' + key + '">'
  string += '<td class="noBorder">'
  string += '<input type="text" name="title" value="' + title + '" readonly>'
  string += '</td>'
  string += '<td class="noBorder">'
  string += '<input type="date" name="due" value="' + due + '" readonly>'
  string += '</td>'
  string += '<td id="completed" class="noBorder">'
  string += '<input type="checkbox" name="key" value="' + key + '" ' + check + '>'
  string += '</td>'
  string += '</tr>'
  string += '<tr class="tableRowSpacer"></tr>'
  return string
}


// TBD: What's the final date format here?
// Input format: 2018-04-05T00:00:00Z
// Output format: 2018-04-05
const parseDate = (datestring) => {
  return datestring.replace(/T.*Z/, '')
}


const taskCount = () => {
  return ($('#todoTable tr:not(".tableRowSpacer")').length - 1)
}
