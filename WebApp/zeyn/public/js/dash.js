$(document).ready( () => {

  $('#todoForm').submit( (event) => {

    const dbRoot = firebase.database()
    const user = firebase.auth().currentUser
    var userId = ''

    try {
      userId = user.uid
    } catch (error) {
      console.log(error)
      userId = 'No user logged in'
    }

    const data = $('form').serializeArray()
    const title = data[1]['value']
    const date = data[2]['value'] + 'T00:00:00Z'

    const todo = {
      'title'       : title,
      'due'         : date,
      'completed'   : 'false',
      'owner'       : userId
    }

    const newTodoKey = dbRoot.ref('todo').push().key

    // You must push the task itself to the /todo/ ref, and the task's id to the user's tasklist in /users/
    var updates = {}
    updates['/todo/' + newTodoKey] = todo
    updates['/users/' + userId + '/tasks/' + newTodoKey] = 'true'

    dbRoot.ref().update(updates)

    const rowHtml = composeTableRow(todo['title'], parseDate(todo['due']), todo['completed'])
    $('#todoTable tbody').append(rowHtml)

    $('#todoForm')[0].reset()

    event.preventDefault()
  })
})
