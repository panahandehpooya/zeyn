$(document).ready( () => {

  const google_provider = new firebase.auth.GoogleAuthProvider()
  var fb_provider = new firebase.auth.FacebookAuthProvider()

  // TODO: Redirect to dashboard after succesful login
  $('#g_login_btn').click(() => {
    firebase.auth().signInWithPopup(google_provider).then( () => {
      window.location = '/dashboard'
    }).catch( (error) => {
      console.log(error.message)
    })
  })


  // TODO: Redirect to dashboard after succesful login
  $('#fb_login_btn').click(() => {
    firebase.auth().signInWithPopup(fb_provider).then( () => {
      window.location = '/dashboard'
    }).catch( (error) => {
      console.log(error.message)
    })

  })

})
