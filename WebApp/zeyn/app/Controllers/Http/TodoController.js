'use strict'

class TodoController {

  // GET the todo page.
  // Other operations are not required (POST, DELETE etc)
  // as operations are handled client side.
  index ({request, view}) {
    return view.render('dashboard')
  }
}

module.exports = TodoController
