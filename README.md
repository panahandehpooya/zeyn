# General notes

- Please read the link below before committing any code
    - https://docs.gitlab.com/ee/README.html

- Commit small and often

- Name branches clearly, e.g.:
    - feat/web-auth
    - bug/bug-description

- Write understandable and descriptive commit messages

- Preface your commit message with web/android/ios respectively
    - "Web: Created login page."
    - "Android: Set up firebase connection."

- Run unit tests before pushing code, don't push broken code

- Write unit tests for new functionality

- Please don't push code directly to master branch
    - Create Merge Request and ask for code reviews
    - Feedback before merging helps improve code quality

# Data

Data is stored to firebase like this:

```
{
    todo: {
        UNIQUE_ID: {
            completed: "false",
            due: "2018-02-27T14:30:00Z",
            owner: "d4d57924-cf1b-45cd-998b-f89a50d87838" // Firebase generated user id
            title: "Example title"
        }
    },
    users: {
        "d4d57924-cf1b-45cd-998b-f89a50d87838" : {
            "tasks" : {
                // The value doesn't matter, just checking the key's existence is enough
                "UNIQUE_ID" : "true"
            }
        }
    }
}
```

### Data types:
- UNIQUE\_ID: Firebase can generate unique id's, use those for todo ids
    - docs here: https://www.firebase.com/docs/web/guide/saving-data.html#section-push

Example in javascript, use the push() function:

``` javascript
// Generate a reference to a new location and add some data using push()
var newPostRef = postsRef.push({
  author: "gracehop",
  title: "Announcing COBOL, a New Programming Language"
});
// Get the unique ID generated by push() by accessing its key
var postID = newPostRef.key;

```

- Completed is just a string stating "true" or "false" in lowercase
- Date uses ISO 8601 standard:
    - 2018-02-26T09:53:29Z
    - After YYYY-MM-DD part the letter T is just a separator, stating
      that the time follows. The timestamp ends with letter Z
    - Pretty much all date/time libraries in all programming languages
      parse dates like this
